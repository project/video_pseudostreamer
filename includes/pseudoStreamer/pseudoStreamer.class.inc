<?php

class pseudoStreamer {

  private $file_handle;
  private $mimeTypeMaps;
  private $inited = FALSE;
  private $active_download;
  
  protected $config = array();

  public function  __construct($config = NULL) {
    $this->mimeTypeMaps = $this->defaultMimeTypeMaps();
    $this->active_download = FALSE;
    
    if ($config) {
      $config += array(
        'seek_start' => 0,
        'seek_end' => -1,
        'file_name' => NULL,
        'file_size' => 0,
        'file_mime' => NULL,
        'content_length' => 0,
        // buffer size in KB
        'buffer_size' => 8,
        'use_http_range' => TRUE,
        'update_callback_func' => NULL,
      );
      $this->setConfig($config);
    }
    register_shutdown_function(array($this, 'onShutdown'));
  }

  protected function onShutdown() {
    $this->fireEvent('stopping', $this->config['update_callback_func']);
  }

  /**
   * Fire a callback event.
   *
   * @param  $event
   * @param  $function
   * @return void
   */
  protected function fireEvent($event, $function) {
    if ($function && function_exists($function)) {
      call_user_func(
        $function,
        $event,
        $this
      );
    }
  }

  protected function init() {
    if ($this->inited) {
      return;
    }

    $this->inited = TRUE;
    $file_path_parts = pathinfo($this->file);
    $this->file_name = $file_path_parts['basename'];
    $this->file_size = $this->content_length = filesize($this->file);
    $file_extension = $file_path_parts['extension'];
    $this->file_mime = isset($this->mimeTypeMaps[$file_extension]) ? $this->mimeTypeMaps[$file_extension] : '';
  }

  /**
   * Begin download.
   *
   * @return void
   */
  public function download() {
    $this->init();

    if (isset($this->position)) {
      $this->seek_start = intval($this->position);
    }

    if (isset($this->buffer_size)) {
      $this->buffer_size = intval($this->buffer_size) * 1024;
    }

    # check http range header
    if ($this->use_http_range) {
      $this->http_range();
      # check seek positions
      if ($this->seek_end < $this->seek_start) {
        $this->seek_end = $this->file_size - 1;
      }
    }

    # set content length
    $this->content_length = $this->seek_end - $this->seek_start + 1;

    # open the file
    if (!$this->file_handle = fopen($this->file, 'rb')) {
      $this->disconnect();
    }

    # output the header
    $this->outputHeaders();

    $this->fseek_file();

    if ($this->partial_download) {
      # print flv header if we have to
      if($this->file_mime == 'video/x-flv') {
        echo 'FLV' , pack('C', 1) , pack('C', 1) , pack('N', 9) , pack('N', 9);
      }
    }


    # get ready for take off
    $speed = 0;
    $bytes_sent = 0;
    $chunk = 1;

    while (($buffer = fread($this->file_handle, (int) $this->buffer_size)) != '') {
      $this->active_download = TRUE;

      #execute a callback to log file download
      $this->fireEvent('downloading', $this->config['update_callback_func']);

      print $buffer;
      flush();

      $bytes_sent += $this->buffer_size;
      $this->bytes_sent = $bytes_sent;
      # make sure we don't read past the total file size
      if ($bytes_sent + $this->buffer_size > $this->content_length) {
//        $this->buffer_size = $this->content_length - $bytes_sent;
      }
    }
    if ($buffer === FALSE) {
      // $buffer returned FALSE
    }
    fclose($this->file_handle);

    $this->disconnect();
  }

  protected function fseek_file() {
    # seek if we have to
    if ($this->seek_start > 0) {
      $this->partial_download = TRUE;
      fseek($this->file_handle, $this->seek_start);
    }
  }

  protected function outputHeaders() {
    header("Content-type: {$this->file_mime}");
    header("Cache-Control: no-cache, no-store, max-age=0, must-revalidate");
    header("Pragma: no-cache");
    header("Accept-Ranges: bytes");
    header("Content-Length: $this->content_length");
    header("Content-Disposition: inline; file_name={$this->file_name};");

    header("Last-Modified: " . date("D, d M Y H:i:s \G\M\T"));
    header("Content-Transfer-Encoding: binary\n");

    if ($this->http_range_download || $this->seek_start) {
      header("HTTP/1.0 206 Partial Content");
      header("Status: 206 Partial Content");
      header("Content-Range: bytes $this->seek_start-$this->seek_end/$this->file_size");
    }

    header("Connection: close");
  }

  # http range
  protected function http_range() {
    if ($this->position > 0) {
      $this->seek_start = $this->position;
      $this->seek_end = $this->file_size;
      $this->partial_download = TRUE;
      $this->http_range_download = TRUE;
    }
    if (isset($_SERVER['HTTP_RANGE'])) {
      $seek_range = substr($_SERVER['HTTP_RANGE'], strlen('bytes='));
      $range = explode('-', $seek_range);
      if ($range[0] > 0) {
        $this->seek_start = intval($range[0]);
      }
      if ($range[1] > 0) {
        $this->seek_end = intval($range[1]);
      } else {
        $this->seek_end = -1;
      }
      $this->partial_download = TRUE;
      $this->http_range_download = TRUE;
    }
  }

  protected function disconnect() {
    exit();
  }

  protected function defaultMimeTypeMaps() {
    return array(
      'flv' => 'video/x-flv',
      'mp4' => 'video/mp4',
      'f4v' => 'video/mp4',
      'f4p' => 'video/mp4',
      'asf' => 'video/x-ms-asf',
      'asr' => 'video/x-ms-asf',
      'asx' => 'video/x-ms-asf',
      'avi' => 'video/x-msvideo',
      'mpa' => 'video/mpeg',
      'mpe' => 'video/mpeg',
      'mpeg' => 'video/mpeg',
      'mpg' => 'video/mpeg',
      'mpv2' => 'video/mpeg',
      'mov' => 'video/quicktime',
      'movie' => 'video/x-sgi-movie',
      'mp2' => 'video/mpeg',
      'qt' => 'video/quicktime'
    );
  }

  public function getDownloadInfo(){
    return array(
      'bytes_sent' => $this->bytes_sent,
      'content_length' => $this->content_length,
    );
  }
  
  public function setConfig($config) {
    $this->config = $config;
  }

  public function __get($key) {
    if (isset($this->config[$key])) {
      return $this->config[$key];
    }
  }

  public function __set($key, $value) {
    $this->config[$key] = $value;
  }

  public function __isset($key) {
    return isset($this->config[$key]);
  }

}

